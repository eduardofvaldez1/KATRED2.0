# ASSUMPTIONS: First condition is ALWAYS numerical and the value to be averaged

import query_builder
import runquery
import count
import summ

global colnum
global colname
global coltype

global condition

file = ""
condition = ""
def build_condition(file, condarray, num, colname, username):
	condition = ""
	temp = condarray[0]
	primary = ""
	for char in temp:
		if char.isalpha():
			primary = primary + char
		else:
			break

	file = open("/var/www/katred_thesis/katred_thesis/Queries/"+username.encode('utf-8')+".fz", "a")
	file.write("\n\nfunction get_condition (line:censusline) : num{")
	query_builder.build_line(file, num, colname)
	file.write("\n\tif")
	for cond in condarray:
	#	condition = condition + cond.translate(None, '"')
		file.write('(' + cond + ')')
		if (cond != condarray[len(condarray)-1]):
			file.write("&&")
			condition = condition + "&&"
	file.write(" then {" + primary + "} else {0.0}\n}")


def query_ave(condarray, highest, sensitivity, steps, username, dataset, colname, coltype):
	global condition
	file = open('/var/www/katred_thesis/katred_thesis/Queries/'+username.encode('utf-8')+'.fz', 'w')
	file.write('#include "library-bags.fz"')
	#query_builder.build_file(file)
	query_builder.build_header(file, coltype)

	file.write("\n\nfunction get_count (line:censusline) : clipped{")
	query_builder.build_line(file, highest, colname)

	count.build_condition(file, condarray)
	file.close()
	build_condition(file, condarray, highest, colname, username)
	file = open("/var/www/katred_thesis/katred_thesis/Queries/"+username.encode('utf-8')+".fz", "a")
	file.write("\nfunction main (db:["+ sensitivity + "] censusline bag) : fuzzy string {")
	file.write("\n\tsample query1_result = fuzz (bagsum (bagmap get_count (clip 0.0) "+ steps +" db));")
	file.write("\n\tsample query2_result = fuzz (bagsum (bagmap get_condition 0 "+steps+" db));")
	file.write('\n\tmsg = (num2string(query2_result / query1_result));')
	#file.write('\n\tmsg =   "Average of '+ condition + ' :"+ (num2string(query2_result)) + "\\n";')
	file.write("\n\treturn msg\n}")
	file.write("\n\nmain")
	file.close()
	runquery.obosen(username, dataset, "Ave")
