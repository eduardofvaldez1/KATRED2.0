import query_builder
import runquery
global colnum
global colname
global coltype

def build_condition(file, condarray):
	temp = condarray[0]
	primary = ""
	for char in temp:
		if char.isalpha():
			primary = primary + char
		else:
			break

	file.write("\tif ")
	for cond in condarray:
		file.write("(" + cond + ")")
		if (cond != condarray[len(condarray)-1]):
			file.write("&&")
	file.write(" then {" + primary + "} else {0.0}\n}")

def query_summ(condarray, highest, sensitivity, steps, username, dataset, colname, coltype):
	file = open('/var/www/katred_thesis/katred_thesis/Queries/'+username.encode('utf-8')+'.fz', 'w')
	file.write('#include "library-bags.fz"')
	#query_builder.build_file(file)
	query_builder.build_header(file, coltype)

	file.write("\n\nfunction get_num (line:censusline) : num {")
	query_builder.build_line(file, highest, colname)

	# condition
	build_condition(file, condarray)
	# Write main function
	file.write("\nfunction main (db:["+sensitivity+"] censusline bag) : fuzzy string {")
	file.write("\n\tsample sum = fuzz (bagsum (bagmap get_num 0 "+steps+" db));")
	file.write("\n\tmsg = num2string(sum);")
	file.write("\n\treturn msg\n}")
	file.write("\n\nmain")
	file.close()
	runquery.obosen(username, dataset, "sum")
