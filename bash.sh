#!/bin/bash

mv query_count.fz ../../../Downloads/fuzz
cd ../../..
cd Downloads/fuzz
./fuzz query_count.fz -o caml-rt/query.ml
cd caml-rt
make
./runquery --mode run --no-root census.db > output.txt
mv output.txt ../../../Desktop/FlaskApp/FlaskApp/Output