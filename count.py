import query_builder
import runquery
import os
global colnum
global colname
global coltype

global condition
condition = ''
def build_condition(file, condarray):
	global condition
	file.write("\nclip (if ")
	for cond in condarray:
		#condition = condition + cond.translate(None, '"')
		file.write('(' + cond + ')')
		if (cond != condarray[len(condarray)-1]):
			file.write("&&")
			condition = condition + "&&"
	file.write(" then {1.0} else {0.0})\n}")

def query_count(condarray, num, sensitivity, steps, username, dataset, colname, coltype):
	file = open('/var/www/katred_thesis/katred_thesis/Queries/'+username.encode('utf-8')+'.fz', 'w')

	#query_builder.build_file(file)
	query_builder.build_header(file, coltype)
	file.write("\n\nfunction query1 (line:censusline) : clipped {")
	query_builder.build_line(file, num, colname)

	build_condition(file, condarray)


	# Write main function

	file.write("\nfunction main (db:["+ sensitivity + "] censusline bag) : fuzzy string {")
	file.write("\n\tsample query1_result = fuzz (bagsum (bagmap query1 (clip 0.0) "+ steps +" db));")
	file.write("\n\tmsg = (num2string(query1_result));")
	file.write("\n\treturn msg\n}")
	file.write("\n\nmain")
	file.close()
	runquery.obosen(username, dataset, "Count")
