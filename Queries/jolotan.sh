#!/bin/bash

mv /var/www/katred_thesis/katred_thesis/Queries/jolotan.fz /var/www/fuzz/Users/jolotan
cd /var/www/fuzz
./fuzz Users/jolotan/jolotan.fz -o Users/jolotan/caml-rt/query.ml
cd Users/jolotan/caml-rt
make
./runquery --mode run --no-root /var/www/fuzz/Users/jolotan/points.db > /var/www/katred_thesis/katred_thesis/Output/jolotan.txt
