#!/bin/bash

mv /var/www/katred_thesis/katred_thesis/Queries/ri.fz /var/www/fuzz/Users/ri
cd /var/www/fuzz
./fuzz Users/ri/ri.fz -o Users/ri/caml-rt/query.ml
cd Users/ri/caml-rt
make
./runquery --mode run --no-root /var/www/fuzz/Datasets/iris_notitle.db > /var/www/katred_thesis/katred_thesis/Output/ri.txt
