#!/bin/bash

mv /var/www/katred_thesis/katred_thesis/Queries/testerman.fz /var/www/fuzz/Users/testerman
cd /var/www/fuzz
./fuzz Users/testerman/testerman.fz -o Users/testerman/caml-rt/query.ml
cd Users/testerman/caml-rt
make
./runquery --mode run --no-root /var/www/fuzz/Users/testerman/points.db > /var/www/katred_thesis/katred_thesis/Output/testerman.txt
