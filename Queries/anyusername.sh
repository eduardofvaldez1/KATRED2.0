#!/bin/bash

mv /var/www/katred_thesis/katred_thesis/Queries/anyusername.fz /var/www/fuzz/Users/anyusername
cd /var/www/fuzz
./fuzz Users/anyusername/anyusername.fz -o Users/anyusername/caml-rt/query.ml
cd Users/anyusername/caml-rt
make
./runquery --mode run --no-root /var/www/fuzz/Users/anyusername/points.db > /var/www/katred_thesis/katred_thesis/Output/anyusername.txt
