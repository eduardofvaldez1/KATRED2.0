#!/bin/bash

mv /var/www/katred_thesis/katred_thesis/Queries/rnisidro.fz /var/www/fuzz/Users/rnisidro
cd /var/www/fuzz
./fuzz Users/rnisidro/rnisidro.fz -o Users/rnisidro/caml-rt/query.ml
cd Users/rnisidro/caml-rt
make
./runquery --mode run --no-root /var/www/fuzz/Users/rnisidro/points.db > /var/www/katred_thesis/katred_thesis/Output/rnisidro.txt
