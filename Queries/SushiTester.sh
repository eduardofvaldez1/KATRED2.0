#!/bin/bash

mv /var/www/katred_thesis/katred_thesis/Queries/SushiTester.fz /var/www/fuzz/Users/SushiTester
cd /var/www/fuzz
./fuzz Users/SushiTester/SushiTester.fz -o Users/SushiTester/caml-rt/query.ml
cd Users/SushiTester/caml-rt
make
./runquery --mode run --no-root /var/www/fuzz/Datasets/ASampleDataset.db > /var/www/katred_thesis/katred_thesis/Output/SushiTester.txt
