#!/bin/bash

mv /var/www/katred_thesis/katred_thesis/Queries/kath.fz /var/www/fuzz/Users/kath
cd /var/www/fuzz
./fuzz Users/kath/kath.fz -o Users/kath/caml-rt/query.ml
cd Users/kath/caml-rt
make
./runquery --mode run --no-root /var/www/fuzz/Datasets/ASampleDataset.db > /var/www/katred_thesis/katred_thesis/Output/kath.txt
