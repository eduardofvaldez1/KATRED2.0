#!/bin/bash

mv /var/www/katred_thesis/katred_thesis/Queries/haifa.fz /var/www/fuzz/Users/haifa
cd /var/www/fuzz
./fuzz Users/haifa/haifa.fz -o Users/haifa/caml-rt/query.ml
cd Users/haifa/caml-rt
make
./runquery --mode run --no-root /var/www/fuzz/Users/haifa/points.db > /var/www/katred_thesis/katred_thesis/Output/haifa.txt
