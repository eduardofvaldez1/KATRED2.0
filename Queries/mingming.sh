#!/bin/bash

mv /var/www/katred_thesis/katred_thesis/Queries/mingming.fz /var/www/fuzz/Users/mingming
cd /var/www/fuzz
./fuzz Users/mingming/mingming.fz -o Users/mingming/caml-rt/query.ml
cd Users/mingming/caml-rt
make
./runquery --mode run --no-root /var/www/fuzz/Datasets/iris_final.db > /var/www/katred_thesis/katred_thesis/Output/mingming.txt
