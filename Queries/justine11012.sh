#!/bin/bash

mv /var/www/katred_thesis/katred_thesis/Queries/justine11012.fz /var/www/fuzz/Users/justine11012
cd /var/www/fuzz
./fuzz Users/justine11012/justine11012.fz -o Users/justine11012/caml-rt/query.ml
cd Users/justine11012/caml-rt
make
./runquery --mode run --no-root /var/www/fuzz/Datasets/ASampleDataset.db > /var/www/katred_thesis/katred_thesis/Output/justine11012.txt
