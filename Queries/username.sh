#!/bin/bash

mv /var/www/katred_thesis/katred_thesis/Queries/username.fz /var/www/fuzz/Users/username
cd /var/www/fuzz
./fuzz Users/username/username.fz -o Users/username/caml-rt/query.ml
cd Users/username/caml-rt
make
./runquery --mode run --no-root /var/www/fuzz/Users/username/points.db > /var/www/katred_thesis/katred_thesis/Output/username.txt
