#!/bin/bash

mv /var/www/katred_thesis/katred_thesis/Queries/SushiMaster.fz /var/www/fuzz/Users/SushiMaster
cd /var/www/fuzz
./fuzz Users/SushiMaster/SushiMaster.fz -o Users/SushiMaster/caml-rt/query.ml
cd Users/SushiMaster/caml-rt
make
./runquery --mode run --no-root /var/www/fuzz/Datasets/ASampleDataset.db > /var/www/katred_thesis/katred_thesis/Output/SushiMaster.txt
