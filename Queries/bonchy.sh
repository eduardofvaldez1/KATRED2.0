#!/bin/bash

mv /var/www/katred_thesis/katred_thesis/Queries/bonchy.fz /var/www/fuzz/Users/bonchy
cd /var/www/fuzz
./fuzz Users/bonchy/bonchy.fz -o Users/bonchy/caml-rt/query.ml
cd Users/bonchy/caml-rt
make
./runquery --mode run --no-root /var/www/fuzz/Users/bonchy/points.db > /var/www/katred_thesis/katred_thesis/Output/bonchy.txt
