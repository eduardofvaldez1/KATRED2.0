#!/bin/bash

mv /var/www/katred_thesis/katred_thesis/Queries/kahitano.fz /var/www/fuzz/Users/kahitano
cd /var/www/fuzz
./fuzz Users/kahitano/kahitano.fz -o Users/kahitano/caml-rt/query.ml
cd Users/kahitano/caml-rt
make
./runquery --mode run --no-root /var/www/fuzz/Users/kahitano/points.db > /var/www/katred_thesis/katred_thesis/Output/kahitano.txt
