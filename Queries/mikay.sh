#!/bin/bash

mv /var/www/katred_thesis/katred_thesis/Queries/mikay.fz /var/www/fuzz/Users/mikay
cd /var/www/fuzz
./fuzz Users/mikay/mikay.fz -o Users/mikay/caml-rt/query.ml
cd Users/mikay/caml-rt
make
./runquery --mode run --no-root /var/www/fuzz/Users/mikay/points.db > /var/www/katred_thesis/katred_thesis/Output/mikay.txt
