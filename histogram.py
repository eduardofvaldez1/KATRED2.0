import query_builder
import runquery


#(allQueries, num1, cols[0].get(), sensitivity.get(), steps.get(), lowerbound, upperbound, interval)
def query_hist(condition, highest, sensitivity, steps, lbound, ubound, interval, username, dataset, colname, coltype):
	file = open("/var/www/katred_thesis/katred_thesis/Queries/"+username+'.fz', 'w')
	file.write ('#include "library-lists.fz"\n')
	
	query_builder.build_header(file, coltype)
	#write hist_aux
	file.write("\n\nfunction hist_aux(c:num) (s:[] num bag) : list(num bag){\n")
	file.write("\tif (c<"+ str(int(ubound)+1) +") then {\n")
	file.write("\t\tlet (y, n) = (bagsplit (fun (z : num) : bool {z<c}) "+steps+" s);\n")
	file.write("\t\tfold[list(num bag)](inr[inner(num bag)](y, hist_aux(c+"+ str(int(interval)) +") n))\n\t}\n")
	file.write("\telse {\n")
	file.write("\t\tfold[list(num bag)](inr[inner(num bag)](s, nil(num bag)))\n\t}\n}")
	
	#write hist function
	file.write("\n\nfunction hist (s:[] num bag) : list(num) {\n\tmap bagsize(hist_aux 0 s)\n}\n")
	
	#write find_column  //find_age
	file.write("\nfunction find_column (line:censusline) : num {")
	query_builder.build_line(file, highest, colname)	
	file.write("\t" + condition + "\n}\n")
	
	#write hist_query_aux function
	file.write("\nfunction hist_query_aux (db:["+sensitivity+"] censusline bag) : list (fuzzy num) { \n\tmap fuzz(hist(bagmap find_column 0 " + steps + " db))\n}\n")
	
	#write sequence function
	file.write("\nfunction sequence (il : [] list(fuzzy num)) : fuzzy list(num) {\n")
	file.write("\tcase unfold il {\n")
	file.write("\t\tinl(y) => return fold[list(num)](inl[inner(num)](()))\n")
	file.write("\t\t|inr(y) => let (h,tl) = y; sample z=h; sample u=(sequence tl);\n")
	file.write("\t\t\treturn fold[list(num)](inr[inner(num)](z,u))\n\t}\n}")
	
	
	#write hist_query function
	file.write("\n\nfunction hist_query (db:["+sensitivity+"] censusline bag) : fuzzy list(num) { \n\tsequence(hist_query_aux db)\n}\n")
	
	#write printer function
	file.write('\nfunction printer (il : list(num)) : string {\n\tcase unfold il {\n\t\tinl(y) => " "\n\t\t|inr(y) => let (h,tl) = y; " "+ (num2string h) + " " + (printer tl)\n\t}\n}\n')
	
	
	# Write main function
	file.write("\nfunction main (db:["+sensitivity+"] censusline bag) : fuzzy string {")
	file.write("\n\tsample histogram = hist_query db;")
	file.write('\n\tmsg = (printer histogram) + "\\n";')
	file.write("\n\treturn msg\n}")
	file.write("\n\nmain\n")
	file.close()
	
	runquery.obosen(username, dataset, "Hist")	
	
	
	
	
