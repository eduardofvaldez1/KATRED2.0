# write and parse data from config file
def build_file(file):
	global colnum
	global colname
	global coltype

	file.write ('#include "library-bags.fz"')

	# Pass config file name from UI
	config = open('configfile2.txt', 'r')

	colnum = int(config.readline())
	colname = config.readline().strip("\r\n")
	coltype = config.readline().strip("\r\n")

	config.close()
	print coltype
	coltype = coltype.split(',')

# header involves the censusline, i.e.  
# typedef censusline = (num,(string,(string,(string,(num,(num,(num,num)))))));
def build_header(file, coltype):
	line = ""
	for cype in coltype:
		line = line + "(" + cype + ","
	line = line + "num" 
	for iter in range(0, len(coltype)):
		line = line + ")"
	line = "\n\ntypedef censusline = " + line + ";\n"
	
	file.write(line)

# builds the let functions, i.e.
# let (age, aage) = line;
# let (sex, asex) = aage;
# let (race, arace) = asex;

def build_line(file, highest, colname):

	for iter in range(0, highest-1):
		line = "\n\tlet (" + colname[iter] + ", a" + colname[iter] + ") = "
		
		if iter == 0:
			line = line + "line;\n"
		else:
			line = line + "a" + colname[iter-1] + ";\n"

		file.write(line)
