import subprocess
import os
import stat
def converttopoints(username):

	file1 = open("/var/www/fuzz/Users/"+username+"/"+username+".sh", "w")
	file1.write("#!/bin/bash\n")
	file1.write("cd /var/www/fuzz/Users/"+username+"/caml-rt\n")
	file1.write("./dbconvert.exe --points /var/www/fuzz/Users/"+username+"/"+username+".csv /var/www/fuzz/Users/"+username+"/points.db")
	file1.close()

	st = os.stat("/var/www/fuzz/Users/"+username+"/"+username+".sh")
	os.chmod("/var/www/fuzz/Users/"+username+"/"+username+".sh", st.st_mode | stat.S_IEXEC)
	test = subprocess.call(".//var/www/fuzz/Users/"+username+"/"+username+".sh")

def converttocensus(username, filename):
	bashname = "/var/www/fuzz/Users/"+username+"/"+username+".sh"

	file1 = open(bashname, "w")
	file1.write("#!/bin/bash\n")
	file1.write("cd /var/www/fuzz/Users/"+username+"/caml-rt\n")
	file1.write("./dbconvert.exe --census /var/www/fuzz/Datasets/"+filename+".csv /var/www/fuzz/Datasets/"+filename+".db")
	file1.close()
	st = os.stat(bashname)
	os.chmod(bashname, st.st_mode | stat.S_IEXEC)
	test = subprocess.call("./"+bashname)
