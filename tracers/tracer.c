#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tracer.h"

#ifdef ENABLE_TRACE_BUFFER
int bufptr;
int bufwrapped;
struct trace_buffer buffer[NUM_ENTRIES_BUFFER];
#endif
#ifdef ENABLE_CYCLE_BUCKETS
struct cycle_bucket buckets[NUM_CYCLE_BUCKETS];
#endif

void init_trace_buffer() {
#ifdef ENABLE_TRACE_BUFFER
  bufptr = 0;
  bufwrapped = 0;
#endif
#ifdef ENABLE_CYCLE_BUCKETS  
  int i;
  for (i=0; i<NUM_CYCLE_BUCKETS; i++) {
    buckets[i].cycles = 0;
    buckets[i].t_start = 0;
    buckets[i].name = NULL;
  }
#endif
}

extern inline unsigned long long rdtsc(void) {
  unsigned long long result;
  unsigned int foo1, foo2;
  asm volatile ("rdtsc\n\t" : "=d" (foo1), "=a" (foo2));
  result = (1ULL<<32)*foo1 + foo2;
  return result;
}

extern inline long long get_bucket(int bucketId) {
  assert((0<=bucketId) && (bucketId<NUM_CYCLE_BUCKETS));
  return buckets[bucketId].cycles;
}

extern inline void set_bucket(int bucketId, long long val, const char *name) {
  assert((0<=bucketId) && (bucketId<NUM_CYCLE_BUCKETS));
  buckets[bucketId].cycles = val;
  
  if (name && !buckets[bucketId].name)
    buckets[bucketId].name = name;
}

extern inline void start_bucket(int bucketId, const char *name) {
  assert((0<=bucketId) && (bucketId<NUM_CYCLE_BUCKETS));
  if (buckets[bucketId].t_start) {
    fprintf(stderr, "Bucket #%d is being started twice\n", bucketId);
    abort();
  }
  buckets[bucketId].t_start = rdtsc();
  if (!buckets[bucketId].name)
    buckets[bucketId].name = name;
}

extern inline void stop_bucket(int bucketId)
{
  assert((0<=bucketId) && (bucketId<NUM_CYCLE_BUCKETS));
  if(buckets[bucketId].t_start)
    buckets[bucketId].cycles += (rdtsc()-buckets[bucketId].t_start);
  buckets[bucketId].t_start = 0;
}

void dump_trace_buffer() {
  int j;
#ifdef ENABLE_TRACE_BUFFER
  char c[5];
  c[4] = 0;
  fprintf(stdout, "Tracebuffer\n");
  int i;
  int first = 0;
  int num = bufptr;
  if (bufwrapped) {
    first = bufptr;
    num = NUM_ENTRIES_BUFFER;
  }
  
  for(j = 0; j < num; j++) {
    i = (j+first) % NUM_ENTRIES_BUFFER;
    *(unsigned int*)&c[0] = buffer[i].id;
    fprintf(stdout, "%lld %lld %s", buffer[i].timestamp, i ? (buffer[i].timestamp-buffer[i-1].timestamp): 0, c);
    if (buffer[i].arg != 0xFFFFFFFF)
      fprintf(stdout, " %d", buffer[i].arg);
    fprintf(stdout, "\n");  
  }
  if (bufwrapped)
    fprintf(stdout, ">=%d entries (buffer wrapped)\n\n", NUM_ENTRIES_BUFFER);
  else
    fprintf(stdout, "%d entries\n\n", bufptr);
  fflush(stdout);
#endif
#ifdef ENABLE_CYCLE_BUCKETS
  fprintf(stdout, "Cycle buckets\n");
  for (j=0; j < NUM_CYCLE_BUCKETS; j++) {
    if (buckets[j].cycles > 0) {
      char namebuf[200];
      if (buckets[j].name)
        strcpy(namebuf, buckets[j].name);
      else
        sprintf(namebuf, "Bucket #%d", j);
      fprintf(stdout, "%-30s %12lld\n", namebuf, buckets[j].cycles);
    }
  }
  fflush(stdout);
#endif  
}

