#ifndef __tracer_h__
#define __tracer_h__

#include <assert.h>
#include <stdio.h>

//define ENABLE_TRACE_BUFFER
#define ENABLE_CYCLE_BUCKETS



void init_trace_buffer();
void dump_trace_buffer();

#ifdef ENABLE_CYCLE_BUCKETS

#define NUM_CYCLE_BUCKETS (100)

extern struct cycle_bucket {
  unsigned long long t_start;
  unsigned long long cycles;
  const char *name;
} buckets[NUM_CYCLE_BUCKETS];


#else
#define start_bucket(x,y) do { } while (0)
#define stop_bucket(x) do { } while (0)
#define get_bucket(x) (0)
#define set_bucket(x, val, name) do { } while (0)
#endif /* defined(ENABLE_CYCLE_BUCKETS) */

#ifdef ENABLE_TRACE_BUFFER

#define NUM_ENTRIES_BUFFER (100000)

extern int bufptr;
extern int bufwrapped;

extern struct trace_buffer {
  unsigned long long timestamp;
  unsigned int id;
  unsigned int arg;
} buffer[NUM_ENTRIES_BUFFER];

#define add_event(xid) \
{ \
  if (bufptr==NUM_ENTRIES_BUFFER) { \
    bufptr = 0; \
    bufwrapped = 1; \
  } \
  \
  buffer[bufptr].timestamp = rdtsc(); \
  buffer[bufptr].id = *(unsigned int*)&(xid);	\
  buffer[bufptr].arg = 0xFFFFFFFF; \
  bufptr ++; \
}

#define add_event2(xid, arga) \
{ \
  if (bufptr==NUM_ENTRIES_BUFFER) { \
    bufptr = 0; \
    bufwrapped = 1; \
  } \
  \
  buffer[bufptr].timestamp = rdtsc(); \
  buffer[bufptr].id = *(unsigned int*)&(xid);	\
  buffer[bufptr].arg = arga; \
  bufptr ++; \
}

#else
#define add_event(xid) do {} while(0)
#define add_event2(xid, arga) do {} while (0)
#endif /* */

#endif /* defined(__tracer_h__) */
