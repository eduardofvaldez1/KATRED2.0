import runquery


def setclusterpts(file, points):

	file.write("\n\nfunction initMeans (x : ()) : pointlist {\n")
	ctr = 1;
	var = ""
	const = ""
	for pts in points:
		var = "point" + str(ctr)
		file.write("\t" + var + " = " + pts + ";\n")
		ctr = ctr + 1
		const = const + "CONS(point, " + var + ", "
	const = const + "NIL(point)"
	for num in range (1, ctr):
		const = const + ")"
	file.write("\t" + const + "\n}")

def query_kmeans(cluster_points, username, dataset):

	file = open("/var/www/katred_thesis/katred_thesis/Queries/"+username+".fz", "w")
	file.write ('#include "library-lists.fz"\n')

	# write type definitions
	file.write("\ntypedef point = (num, num);")
	file.write("\ntypedef pointlist = mu X => (() + (point,X));")
	file.write("\ntypedef labpointbag = (point,num) bag;")
	file.write("\ntypedef pointbag = (num, num) bag;")

	# write totx
	file.write("\n\nfunction totx (db :[] pointbag) : fuzzy num { ")
	file.write("\n\tfuzz (bagsum ((bagmap (fun (p:point) : clipped {let(x,y)=p; clip x})) (clip 0.5) 28.0 db)) ")
	file.write("\n}")

	# write toty
	file.write("\n\nfunction toty (db :[] pointbag) : fuzzy num {")
	file.write("\n\tfuzz (bagsum ((bagmap (fun (p:point) : clipped {let(x,y)=p; clip y})) (clip 0.5) 27.0 db))")
	file.write("\n}")

	# write tot
	file.write("\n\nfunction tot  (db :[] pointbag) : fuzzy num {")
	file.write("\n\tfuzz (bagsum ((bagmap (fun (p:point) : clipped { clip 1 })) (clip 0.5) 33.0 db)) ")
	file.write("\n}")

	# write zip
	file.write("\n\nfunction zip (l1 : list(A)) (l2 : list(B)) : list((A,B)) {")
	file.write("\n\tcase unfold l1 {")
	file.write("\n\tinl(x) => NIL((A,B))")
	file.write("\n\t| inr(x) => case unfold l2 {")
	file.write("\n\t\tinl(y) => NIL((A,B))")
	file.write("\n\t\t| inr(y) => let (xh,xt) = x; let (yh,yt) = y; ")
	file.write("\n\t\t\tCONS((A,B),(xh,yh),((zip xt) yt))")
	file.write("\n\t\t}")
	file.write("\n\t}")
	file.write("\n}")

	# write avg
	file.write("\n\nfunction avg (pi : (point, num)) : (num,num) {")
	file.write("\n\tlet (p, i) = pi;")
	file.write("\n\tlet (x, y) = p;")
	file.write("\n\t(x / i, y / i)\n}")

	# write sqdist
	file.write("\n\nfunction sqdist (p1 : point) (p2 : point) : num {")
	file.write("\n\tlet (x1,y1) = p1;")
	file.write("\n\tlet (x2,y2) = p2;")
	file.write("\n\tdx = x2 - x1;")
	file.write("\n\tdy = y2 - y1;")
	file.write("\n\tdx * dx + dy * dy")
	file.write("\n}")

	# write argminaux
	file.write("\n\nfunction argminaux (best : num) (bestIndex : num) (curIndex:num) (x : list(num)) : num {")
	file.write("\n\tcase unfold x {")
	file.write("\n\tinl(y) => bestIndex")
	file.write("\n\t| inr(y) => let (h,tl) = y; if (h < best) then { argminaux h curIndex (curIndex+1) tl }")
	file.write("\n\t\telse { argminaux best bestIndex (curIndex+1) tl }")
	file.write("\n\t}")
	file.write("\n}")

	# write argmin
	file.write("\n\nfunction argmin (x : list(num)) : num {")
	file.write("\n\targminaux 1000000 0 0 x")
	file.write("\n}")

	# write assign
	file.write("\n\nfunction assign (means : pointlist) (db :[] pointbag) : ((point,num) bag) {")
	file.write("\n\t(bagmap (fun (pt : point) : (point, num) {")
	file.write("\n\tsqdists = pubmap (fun (pt2 : point) : num { sqdist pt pt2 }) means;")
	file.write("\n\t(pt, argmin sqdists)")
	file.write("\n\t})) ((0.0, 0.0), 0.0) 45.0 db")
	file.write("\n}")

	# write partition
	file.write("\n\nfunction partition (ldb :[] labpointbag) (m:num) (n:num) : list((point, num) bag) {")
	file.write("\n\tif (m == n) then { NIL((point,num) bag) }")
	file.write("\n\telse {")
	file.write("\n\t\tlet (yes, no) = ((bagsplit (fun (x:(point,num)) : bool { let (pt,i)=x; i == m })) 26.0 ldb);")
	file.write("\n\t\tCONS((point,num) bag, yes, partition no (m+1) n)")
	file.write("\n\t}")
	file.write("\n}")

	# write iterate
	file.write("\n\nfunction iterate (db :[3] pointbag) (means : pointlist) : fuzzy pointlist {")
	file.write("\n\tldb = ((assign means) db);")
	file.write("\n\tp = (((partition ldb) 0) (length means));")
	file.write("\n\tdb2 = ((map (bagmap (fun (x:(point,num)) : point { let (pt,n) = x;  pt} ) (0.0, 0.0) 29.0 )) p);")
	file.write("\n\tsample tx = listfuzz (map totx db2);")
	file.write("\n\tsample ty = listfuzz (map toty db2);")
	file.write("\n\tsample t = listfuzz (map tot db2);")
	file.write("\n\tinfo = zip (zip tx ty) t;")
	file.write("\n\treturn (pubmap avg info)")
	file.write("\n}")

	# write initmeans
	setclusterpts(file, cluster_points)
	# write report
	file.write('\n\nfunction report (il : pointlist) : string {')
	file.write('\n\tcase unfold il {')
	file.write('\n\t\tinl(u) => ""')
	file.write('\n\t\t|inr(p) =>')
	file.write('\n\t\tlet (elem,rest) = p;')
	file.write('\n\t\tlet (x, y) = elem;')
	file.write('\n\t\t"(" + (num2string x) + "," + (num2string y) + ")\\n" + (report rest)')
	file.write('\n\t}')
	file.write('\n}')

	# write fun
	file.write('\n\nfun (db :[6] pointbag) : fuzzy string {')
	file.write('\n\tx = (initMeans ());')
	file.write('\n\tsample foo = iterate db x;')
	file.write('\n\tsample foo2 = iterate db foo;')
	file.write('\n\treturn (report (foo2))')
	file.write('\n}')

	file.close()
	runquery.obosen(username, dataset, "Kmeans")

