import subprocess
import os
import stat

def create_user(username):
	filename = "/var/www/katred_thesis/katred_thesis/Creates/" + username + ".sh"
	file = open(filename, 'w')
	file.write("#!/bin/bash\n\n mkdir /var/www/fuzz/Users/"+username)
	file.write("\n cp -a /var/www/fuzz/Users/caml-rt /var/www/fuzz/Users/"+username)
	file.write("\n cp /var/www/fuzz/queries/library-bags.fz /var/www/fuzz/Users/"+username)
	file.write("\n cp /var/www/fuzz/queries/library-lists.fz /var/www/fuzz/Users/"+username)
	file.close()
	st = os.stat(filename)
	os.chmod(filename, st.st_mode | stat.S_IEXEC)
	test = subprocess.call("./"+filename)
