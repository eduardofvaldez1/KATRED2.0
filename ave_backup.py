import query_builder
import runquery
import count

global colnum
global colname
global coltype

global condition

file = ""
condition = ""
def build_condition(file, condarray, num, strr):
	global condition
	file = open("query_ave.fz", "a")
	file.write("\n\nfunction get_condition (line:censusline) : num{")
	query_builder.build_line(file, num)
	file.write("\n\tif")
	for cond in condarray:
		condition = condition + cond.translate(None, '"')
		file.write('(' + cond + ')')
		if (cond != condarray[len(condarray)-1]):
			file.write("&&")
			condition = condition + "&&"
	file.write(" then {" + strr+ "} else {0.0}\n}")


def query_ave(condarray, num, strr, sensitivity, steps):
	global condition
	file = open('query_ave.fz', 'w')
	
	query_builder.build_file(file)
	query_builder.build_header(file)
	
	file.write("\n\nfunction get_count (line:censusline) : clipped{")
	query_builder.build_line(file, num)
	
	count.build_condition(file, condarray)
	file.close()
	build_condition(file, condarray, num, strr)
	file = open("query_ave.fz", "a")
	file.write("\nfunction main (db:["+ sensitivity + "] censusline bag) : fuzzy string {")
	file.write("\n\tsample query1_result = fuzz (bagsum (bagmap get_count (clip 0.0) "+ steps +" db));")
	file.write("\n\tsample query2_result = fuzz (bagsum (bagmap get_condition 0 "+steps+" db));")
	file.write('\n\tmsg =   "Average of '+ condition + ' :"+ (num2string(query2_result / query1_result)) + "\\n";')
	#file.write('\n\tmsg =   "Average of '+ condition + ' :"+ (num2string(query2_result)) + "\\n";')
	file.write("\n\treturn msg\n}")
	file.write("\n\nmain")
	file.close()
	runquery.obosen("average")
	
