import subprocess
import os
import stat
def anoname(type):
	if type == "sum":
		return "query_sum.fz"
	elif type == "count":
		return "query_count.fz"
	elif type == "average":
		return "query_ave.fz"
	elif type == "histogram":
		return "query_hist.fz"
	elif type == "kmeans":
		return "query_kmeans.fz"
	else:
		print "Type not valid"

def obosen(username, dataset, querytype):

	#if random_type != "kmeans":
	bashname = "/var/www/katred_thesis/katred_thesis/Queries/"+username+".sh"
	filename = "/var/www/katred_thesis/katred_thesis/Queries/"+username+".fz"
	queryname = "/var/www/fuzz/Users/"+username+".fz"

	file = open(bashname, 'w')
	file.write("#!/bin/bash\n\nmv "+filename+" /var/www/fuzz/Users/"+username)
	file.write("\ncd /var/www/fuzz\n")
	file.write("./fuzz Users/"+ username + "/"+ username +'.fz' + " -o Users/"+username+"/caml-rt/query.ml\n")
	
	if querytype == "Kmeans":
		file.write("cd Users/"+username+"/caml-rt\nmake\n./runquery --mode run --no-root /var/www/fuzz/Users/"+username+"/points.db > /var/www/katred_thesis/katred_thesis/Output/"+username+".txt\n")
	else:
		file.write("cd Users/"+username+"/caml-rt\nmake\n./runquery --mode run --no-root /var/www/fuzz/Datasets/"+dataset+".db > /var/www/katred_thesis/katred_thesis/Output/"+username+".txt\n")
	#file.write("mv output.txt ../../katred_thesis/katred_thesis/Output")
	file.close()
	st = os.stat(bashname)
	os.chmod(bashname, st.st_mode | stat.S_IEXEC)
	test = subprocess.call("./"+bashname)
