from flask import Flask,render_template,request,jsonify,session,redirect,url_for
#from flaskext.mysql import MySQL
from random import sample
from models import db, user, dataset, configfile, col, queries, choices
import bcrypt
import user_create
#user will have his/her own table of queries
from math import *
import os
import count
import ave
import summ
import kmeans
#import kpoints
import histogram
import json
import pygal
import csvpoints
global curr_user

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:differential@localhost/katred_thesis'
UPLOAD_DIR = 'datasets/'
app.config['UPLOAD_FOLDER'] = UPLOAD_DIR

db.init_app(app)

app.secret_key="\xe2@\x8b\x06\x13F\xcev\xbf`\xce\xab\xc8\xc6\n\x88\x16\xac\xb9\xa0\xc0\xfa\xd3\xd2"
#routes

@app.route('/histogramdata')
def histogramdata():
	histogram_data=queries.query.filter_by(username=session['username'],querytype="Hist").all() #get all
	histogram_data=histogram_data[-1] #get last occurence
	temp=histogram_data.querycond.split("|")
	#queryans of histogram looks like
	#"[12.23,30,30.2]"
	#querycondition of histogram looks like
	# "column|lowerbound|Upperbound|Interval"
	"Age|0|100|24"
	bins=[]
	bins=histogram_data.queryans
	#bins is "[10,20,30]"
	bins=bins.replace("\r","").replace("\n","")
	bins=bins.replace("[","").replace("]","")
	bins=bins.split(",")
	bins=map(float,bins)
	title=str(temp[0])
	lowerbound=float(temp[1])
	upperbound=float(temp[2])
	interval_width=float(temp[3])
	intervals=[] #list of string intervals
	number_of_intervals=int(floor((upperbound-lowerbound)/interval_width))
	temp=lowerbound
	print number_of_intervals
	for i in range(number_of_intervals):
		intervals.append(str(temp)+"-"+str(temp+interval_width))
		temp=temp+interval_width
        bins=bins[:number_of_intervals]
        print "bins",bins
	print "intervals",intervals
	print "title",title
	print {"title":title,"intervals":intervals,"bins":bins}
	return jsonify({"title":title,"intervals":intervals,"bins":bins})

@app.route('/kmeansdata')
def kmeansdata():
	print "coldplay1"
	#the queryans of Kmeans looks like points separated by newlines
	#"(1,2)\n(3,4)""
	#the querycond looks like
	#"column1,column2|(1,2)\n(3,4)"
	
	#kmeans_data=queries.query.filter_by(username=session['username'],querytype="Kmeans").first() #Assuming the latest is the first on the DB
	#FOR TESTING PURPOSES,WE GET ALL QUERIES THEN GET LAST
	kmeans_data=queries.query.filter_by(username=session['username'],querytype="Kmeans").all() #get all
	kmeans_data=kmeans_data[-1] #get last occurence
	temp=kmeans_data.querycond.split("|")
	#temp[0] contains the two columns temp[1] contains the points input by user
	conditions=temp[0].replace(","," AND ")
	points=temp[1].replace(" ","")
	#points=points.replace(" ","")
        points=points.replace(")(",",").replace("(","").replace(")","")
	points=points.split(",") #creates a 1D array for all points ["1","2","3","4"]
	points=map(float,points)
	pairs_of_points=[list(points[i:i+2]) for i in range(0,len(points),2)]
	#centers=kmeans_data.queryans.replace("\r","").replace("\n","") #removes newlines and concatenates (1,2)(3,4)(5,6)
	centers=kmeans_data.queryans.replace(" ","")
        centers=centers.replace(")(",",").replace("(","").replace(")","")
	centers=centers.split(",") #creates a 1D array for all centers [1,2,3,4]
	centers=map(float,centers)
	pairs_of_centers=[list(centers[i:i+2]) for i in range(0,len(centers),2)]
	#check
	print "points: ",pairs_of_points
	print "centers: ",pairs_of_centers #result should be[[1,2],[3,4]]
	#temp[0] contains the two columns temp[1] contains the points input by user
 	return jsonify({"title":conditions,"centers":pairs_of_centers, "points":pairs_of_points})

@app.route('/countdata')
def countdata():
	#global curr_user
	print "coldplay1"
	#if count , select * from results database where user == X and query type == average
	# Must JSONIFY { : Fuzz Result , Corresponding Query Executed }
	count_queries=[]
	results=[]
	xaxistitles=[]
	count_queries=queries.query.filter_by(username=session['username'],querytype="Count").all()
	print count_queries
	for count_query in count_queries:
		results.append(float(count_query.queryans))
		xaxistitles.append(count_query.querycond)
	print "results",results
	print "xaxis",xaxistitles
	#return jsonify({"results":sample(range(1,1000),5,"XAxisTitles":['age=2', 'sex=M', 'age>20', 'sex=M', 'age<1']})

	return jsonify({"results":results,"XAxisTitles":xaxistitles})

@app.route('/sumdata')
def sumdata():
	#global curr_user
	print "coldplay2"
	#if sum , select * from results database where user == X and query type == average
	# Must JSONIFY { : Fuzz Result , Corresponding Query Executed }
	count_queries=[]
	results=[]
	xaxistitles=[]
	count_queries=queries.query.filter_by(username=session['username'],querytype="Sum").all()
	for count_query in count_queries:
			print "lea"
			print float(count_query.queryans)
			results.append(float(count_query.queryans))
			xaxistitles.append(count_query.querycond)
	print "results",results
	print "xaxis",xaxistitles
	return jsonify({"results":results,"XAxisTitles":xaxistitles})

@app.route('/avedata')
def avedata():
	#global curr_user
	print "coldplay3"
	#if average , select * from results database where user == X and query type == average
	# Must JSONIFY { : Fuzz Result , Corresponding Query Executed }
	count_queries=[]
	results=[]
	xaxistitles=[]
	count_queries=queries.query.filter_by(username=session['username'],querytype="Ave").all()
	for count_query in count_queries:
			results.append(float(count_query.queryans))
			xaxistitles.append(count_query.querycond)
	print "results",results
	print "xaxis",xaxistitles
	return jsonify({"results":results,"XAxisTitles":xaxistitles})




@app.route('/', methods=['GET']) #homepage
def index():
	return render_template('login.html')

# login, insert to db
@app.route('/login', methods=['POST'])
def insert():
	global curr_user
	if request.form.get('login')=='signup':
		test_user = user.query.filter_by(username=request.form.get("username")).first()
		if test_user is None:
			test = user(request.form.get("username"), bcrypt.hashpw(request.form.get("password").encode('utf-8'),bcrypt.gensalt()))
			db.session.add(test)
			db.session.commit()
			session['username'] = request.form.get('username')
			user_create.create_user(request.form.get('username'))
			return redirect(url_for('viewhelp'))
		else:
			return render_template('login.html', error='exists')
	else:
		test_user = user.query.filter_by(username=request.form.get("username")).first()
		if test_user is not None and bcrypt.hashpw(request.form.get("password").encode('utf-8'), test_user.password.encode('utf-8')) == test_user.password.encode('utf-8'):
			session['username'] = request.form.get('username')
		else:
			return render_template('login.html', error = 'error')
	session['status_cas'] = 'none'
	session['status_hist'] = 'none'
	session['status_kmeans'] = 'none'
	session['status_upload'] = 'none'
	return redirect(url_for('viewmain'))

@app.route('/home')
def viewmain():
	if 'username' not in session:
			return redirect(url_for('index'))
	session['status_hist'] = 'none'
	session['status_kmeans'] = 'none'
	session['status_upload'] = 'none'
	
	#paths = [i for sub in paths for i in sub]
	datasets = dataset.query.all()
	columns = col.query.all()
	choice = choices.query.all()
	filedict = {}
	dict = {}
	
	for data in datasets:									# iterate through datasets,
		filedict[data.title] = data.description
		cols = []
		for column in columns: 								# append all columns to array whose title matches with the dataset title
			if column.title == data.title:
				cols.append(column.columnname)
		dict[data.title] = cols
	#get columns for dataset
	query_graphs=pygal.HorizontalBar()
	# pass dict as JSON
	return render_template('main.html', datasets = datasets, columns_dict = json.dumps(dict), data_dict = json.dumps(filedict), status=session['status_cas'])#,query_graphs=query_graphs)



@app.route('/query', methods=['POST'])
def fuzz_query():
	if request.method == 'POST':
		filename = request.form.get("filename")
		sensitivity = request.form.get("sensitivity")
		numberofsteps = request.form.get("numberofsteps")
		counter = int(request.form.get("rowCounter"))			# how many rows in main page
		firstcolumn = str(request.form.get("column1"))
		
		# Check if column type is valid
		checkCol = col.query.filter_by(title=filename,columnname=request.form.get("column1")).first()
		if request.form.get("submit_query") != 'Count':
			if checkCol.columntype != 'num':
				session['status_cas'] = 'numError'
				return redirect(url_for('viewmain'))
		
		num = 0
		allQueries=[] #String containing all queries
		#filename = request.form.get("filename")
		todb = ""
        #filename = ['age','sex','race','union','zipcode','income'] # configfile
		highest = 0

		# get all columns in dataset
		coldict = {}
		collist = col.query.filter_by(title=filename).all()
		for column in collist:
			coldict[column.columnname] = column.columntype

		for i in range(1,counter+1):
			index = col.query.filter_by(title=filename,columnname=request.form.get("column"+str(i))).first()
			
			# Check if column is index, then argument should always be ==
			if index.columntype == 'string':
				if str(request.form.get("operation"+str(i))) != "==":
					session['status_cas'] = 'opError'
					return redirect(url_for('viewmain'))
			else:
				try:
					float(request.form.get("argument"+str(i)))
				except:
					session['status_cas'] = 'typeError'
					return redirect(url_for('viewmain'))
			# Check if choice is in database
			checkChoice = choices.query.filter_by(column_id=index.id).all()
			if len(checkChoice) != 0:
				checkIter = 0
				for choice in checkChoice:
					if request.form.get("argument"+str(i)) == choice.choicename:
						checkIter = 1
				if checkIter == 0:
					session['status_cas'] = 'choiceError'
					return redirect(url_for('viewmain'))
			
			if index.columnindex > highest:
				highest = index.columnindex

			if coldict[str(request.form.get("column"+str(i)))] == 'string':
				query = str(request.form.get("column"+str(i))) + str(request.form.get("operation"+str(i))) + '"'+ str(request.form.get("argument"+str(i)))+'"'
			else:
				query = str(request.form.get("column"+str(i))) + str(request.form.get("operation"+str(i))) + str(request.form.get("argument"+str(i)))
			allQueries.append(query)
			if i == 1:
				todb = query
			elif i == 2:
				todb = todb + " where " + query
			else:
				todb = todb + " " + query + " "

		# pass column list and column type
		colname = []
		coltype = []
		columns = col.query.filter_by(title=filename).all()
		for column in columns:
			colname.append(column.columnname)
			coltype.append(column.columntype)

		if request.form.get("submit_query") == 'Count':
			print "count"
			count.query_count(allQueries, highest+1, sensitivity, numberofsteps, session['username'], filename, colname, coltype)
		elif request.form.get("submit_query") == 'Sum':
			print "sum"
		        summ.query_summ(allQueries, highest+1, sensitivity, numberofsteps, session['username'], filename, colname, coltype)
		else:
			print "ave"
			ave.query_ave(allQueries, highest+1, sensitivity, numberofsteps, session['username'], filename, colname, coltype)


		# get answer from txt file
		fileans = open("/var/www/katred_thesis/katred_thesis/Output/"+session['username']+".txt", 'r')
		temp = fileans.readline().strip("\r\n")
		fileans.close()
		answer = ""
		for char in temp:
			if char.isdigit() or char =='.':
				answer = answer + char
			else:
				if answer == "":
					answer = 0
				break
		# add query to database
		if answer != 0:
			query = queries(filename, session['username'], todb, request.form.get("submit_query"), answer, sensitivity, numberofsteps)
			db.session.add(query)
			db.session.commit()
			session['status_cas'] = answer

			session['status_hist'] = 'none'
			session['status_kmeans'] = 'none'
			session['status_upload'] = 'none'
			return redirect(url_for('viewmain'))
		session['status_cas'] = 'error'
		return redirect(url_for('viewmain'))

# upload file page
@app.route('/upload')
def upload():
	if 'username' not in session:
		return redirect(url_for('index'))
	session['status_cas'] = 'none'
	session['status_hist'] = 'none'
	session['status_kmeans'] = 'none'
	datasets=dataset.query.all()
	filenames_dict = {}
	existing_filenames=[]
	# return a dictionary of lists of dictionary, where the outer dictionary has dataset titles as key, and the inner dictionary contains the columns for the column models
	# {Dataset 1: [{Column 1: Age, Column 1 Type: Number}, {Column 2: Sex, Column 2 Type: String}], Dataset 2:[{}]}
	for data in datasets:
		existing_filenames.append(data.title)
	print existing_filenames
	filenames_dict["filenames"]=existing_filenames
	print filenames_dict
	#return render_template('edit.html', datasets = datasets, dataset_dict = json.dumps(dataset_dict), columns_dict = json.dumps(columns_dict))
	print json.dumps(filenames_dict)
	return render_template('upload.html',filenames=json.dumps(filenames_dict), status = session['status_upload'])

# upload dataset and config file to database
def check_filename(filename):
	datasets = dataset.query.all()
	for dataset in datasets:
		if dataset.title == filename:
			return False
	return True
@app.route('/upload_file',methods=['POST'])
def upload_file():
	global curr_user
	datasets = dataset.query.all()
	columnList=[] #list of list for all columns  [ [column name, type,options] ]
	#options=[]
	print datasets
	for d in datasets:
		print d.title
	if request.method=='POST':
		description = request.form.get('uploadfdescription')
		filename = request.form.get('uploadfname')
		counter = int(request.form.get("rowCounter")) #gets how many rows is in upload.html
		
		f=request.files['file'] #fetches file form request.file[] object and saves it to desired location
		f.save(os.path.join("/var/www/fuzz/Datasets",filename+".csv")) #saves relative file path
		#input_dataset = dataset(filename, description, "Di ko alam yung path haha", "Hardcoded muna")
		

		column_id = col.query.filter_by(title=filename).first()

		if column_id==None: #if filename is unique
			input_dataset = dataset(filename, description, "Di ko alam yung path haha", session['username'])
			db.session.add(input_dataset)
			db.session.commit()
			input_config = configfile(filename,counter)
			db.session.add(input_config)
			db.session.commit()
			
			for i in range(1,counter+1):
				input_columns=col(filename,request.form.get('col'+str(i)),request.form.get('col-type'+str(i)),i)
				db.session.add(input_columns)
				db.session.commit()
				
				input_choices=request.form.get('options'+str(i)).split(",")
				if input_choices[0] != "":
					cid = col.query.filter_by(title=filename,columnname=request.form.get('col'+str(i))).first()
					for c in input_choices:
						db.session.add(choices(cid.id, c))
						db.session.commit()
		csvpoints.converttocensus(session['username'], filename)

		# for config file input, uncomment na lang
		#input_config = configfile(filename,counter)
		#db.session.add(input_config)
		#db.session.commit()

		#for i in range(1, counter+1):
		#	title = request.form.get('col' + str(i))
		#	type = request.form.get('col-type' + str(i))
		#	input_column = col(filename, title, type)
		#	db.session.add(input_column)
		#	db.session.commit()
		#print len(columnList)       #how many columns
		print list(columnList)
		datasets=dataset.query.all()
		filenames_dict = {}
		existing_filenames=[]
		# return a dictionary of lists of dictionary, where the outer dictionary has dataset titles as key, and the inner dictionary contains the columns for the column models
		# {Dataset 1: [{Column 1: Age, Column 1 Type: Number}, {Column 2: Sex, Column 2 Type: String}], Dataset 2:[{}]}
		for data in datasets:
			existing_filenames.append(data.title)
		print existing_filenames
		filenames_dict["filenames"]=existing_filenames
		print filenames_dict
		#return render_template('edit.html', datasets = datasets, dataset_dict = json.dumps(dataset_dict), columns_dict = json.dumps(columns_dict))
		session['status_upload'] = "success"
		#return str(columnList) #config file list within a list
		#return render_template('upload.html',filenames=json.dumps(filenames_dict))
                return redirect(url_for('upload'))
#route for editing config files
@app.route('/edit',methods=['GET','POST'])
def edit():
	if 'username' not in session:
		return redirect(url_for('index'))
	session['status_cas'] = 'none'
	session['status_hist'] = 'none'
	session['status_kmeans'] = 'none'
	session['status_upload'] = 'none'
	datasets = dataset.query.filter_by(owner=session['username']).all()
	columns = col.query.all()
	dataset_dict = {}
	columns_dict = {}
	# return a dictionary of lists of dictionary, where the outer dictionary has dataset titles as key, and the inner dictionary contains the columns for the column models
	# {Dataset 1: [{Column 1: Age, Column 1 Type: Number}, {Column 2: Sex, Column 2 Type: String}], Dataset 2:[{}]}
	for data in datasets:
		dataset_dict[data.title] = [data.description, data.path, data.owner]
		columns_dict[data.title]=[column.serialize() for column in columns if column.title == data.title]

	return render_template('edit.html', datasets = datasets, dataset_dict = json.dumps(dataset_dict), columns_dict = json.dumps(columns_dict))

@app.route('/edit_file', methods=['POST'])
def edit_file():
    global curr_user

    columnList=[] #list of list for all columns  [ [column name, type] ]

    if request.method=='POST':
		description = request.form.get('editfdescription')
		filename = request.form.get('editfname')

		counter = int(request.form.get("rowCounter")) #gets how many rows is in upload.html

		# dataset edit
		# query the original title of dataset
		input_dataset = dataset.query.filter_by(title=request.form.get('orig')).first()
		input_dataset.title = filename
		input_dataset.description = description
		db.session.commit()

		# config file edit
		input_config = configfile.query.filter_by(title=filename).first()
		input_config.colnum = counter
		db.session.commit()

		# column edit
		for i in range(1, counter+1):
			input_column = col.query.filter_by(id = request.form.get('hid' + str(i))).first()
			if input_column:
				input_column.columnname = request.form.get('col' + str(i))
				input_column.columntype = request.form.get('col-type' + str(i))
			else:
				input_column = col(filename, request.form.get('col' + str(i)), request.form.get('col-type' + str(i)))
				db.session.add(input_column)
			db.session.commit()

		return redirect(url_for('viewmain'))

@app.route('/histogrampage')
def histogram_page():
	if 'username' not in session:
		return redirect(url_for('index'))
	session['status_cas'] = 'none'
	session['status_kmeans'] = 'none'
	session['status_upload'] = 'none'
	datasets = dataset.query.all()
	columns = col.query.all()
	dict = {}
	for data in datasets:
		cols = []
		for column in columns:
			if column.title == data.title:
				cols.append(column.columnname)
		dict[data.title] = cols

	return render_template('histogram.html', datasets = datasets, columns_dict = json.dumps(dict), status=session['status_hist'])


@app.route('/makehistogram', methods=['POST'])
def histogram_query():
	if request.method == 'POST':
		filename = request.form.get("filename")
		hist_column = request.form.get("column1")
		
		# Check if primary column is num
		checkCol = col.query.filter_by(title=filename,columnname=hist_column).first()
		if checkCol.columntype != 'num':
			session['status_hist'] = 'numError'
			return redirect(url_for('histogram_page'))
		
		sensitivity = request.form.get("sensitivity")
		numberofsteps = request.form.get("numberofsteps")
		
		bound_lower = request.form.get("lbound")
		bound_upper = request.form.get("ubound")
		
		if int(bound_lower) >= int(bound_upper):
			session['status_hist'] = 'boundError'
			return redirect(url_for('histogram_page'))
	
		intervals = request.form.get("interval")
		if (int(intervals)) <= 0 or (int(bound_upper)-int(bound_lower)<int(intervals)):
			
			session['status_hist'] = 'intervalError'
			return redirect(url_for('histogram_page'))
		# get highest index for column
		highest = checkCol.columnindex + 1

		# get columns and column type from db
		colname = []
		coltype = []
		columns = col.query.filter_by(title=filename).all()

		for column in columns:
			colname.append(column.columnname)
			coltype.append(column.columntype)

		histogram.query_hist(hist_column, highest, sensitivity, numberofsteps, bound_lower, bound_upper, intervals, session['username'], filename, colname, coltype)

		# get answer from txt file
		fileans = open("/var/www/katred_thesis/katred_thesis/Output/"+session['username']+".txt", 'r')
		temp = fileans.readline().strip("\r\n")
		fileans.close()
		if temp[0] != "C":
			temp2 = temp.split("  ")
			answer = []
			for interval in temp2:
				if ((interval != temp2[0]) and (interval!='')):
					answer.append(float(interval))
			query = queries(filename, session['username'], hist_column+"|"+str(bound_lower)+"|"+str(bound_upper)+"|"+str(intervals), request.form.get("submit_query"), str(answer), sensitivity, numberofsteps)
			db.session.add(query)
			db.session.commit()
			
			session['status_cas'] = 'none'
			session['status_hist'] = 'success'
			session['status_kmeans'] = 'none'
			session['status_upload'] = 'none'
			return redirect(url_for('histogram_page'))
		session['status_hist'] = 'error'
		return redirect(url_for('histogram_page'))

@app.route('/kmeanspage')
def kmeans_page():
	session['status_cas'] = 'none'
	session['status_hist'] = 'none'
	session['status_upload'] = 'none'
	if 'username' not in session:
		return redirect(url_for('index'))
	datasets = dataset.query.all()
	columns = col.query.all()
	dict = {}
	for data in datasets:
		cols = []
		for column in columns:
			if column.title == data.title:
				cols.append(column.columnname)
		dict[data.title] = cols

	return render_template('kmeans.html', datasets = datasets, columns_dict = json.dumps(dict), status=session['status_kmeans'])


@app.route('/makekmeans', methods=['POST'])
def kmeans_query():
	if request.method == 'POST':
		filename = request.form.get("filename")
		column_x = request.form.get("columnx")
		column_y = request.form.get("columny")
		# Check if both columns are numbers
		columnX = col.query.filter_by(title=filename, columnname=column_x).first()
		columnY = col.query.filter_by(title=filename, columnname=column_y).first()
		if ((columnX.columntype != 'num') or (columnY.columntype != 'num')):
			session['status_kmeans'] = 'numError'
			return redirect(url_for('kmeans_page'))
		
		sensitivity = request.form.get("sensitivity")
		numberofsteps = request.form.get("numberofsteps")
		counter = int(request.form.get("rowCounter"))
		

		column_x_index = columnX.columnindex
		column_y_index = columnY.columnindex

		file1 = open("/var/www/fuzz/Datasets/"+filename+".csv", "r")
		file2 = open("/var/www/fuzz/Users/"+session['username']+"/"+session['username']+".csv", "w")
		fileline = file1.readline()
		while fileline != "":
			line = fileline.split(",")
			file2.write(line[column_x_index-1]+"\t"+line[column_y_index-1]+"\n")
			fileline = file1.readline()
		file1.close()

		file2.close()

		csvpoints.converttopoints(session['username'])
		pointlist = []
                pointstr =""
		for i in range(1,counter+1):
			point = "(" + str(request.form.get("x"+str(i))) + "," + str(request.form.get("y"+str(i))) + ") "
			pointlist.append(point)
                        pointstr = pointstr + point

		kmeans.query_kmeans(pointlist, session['username'], filename)


		fileans = open("/var/www/katred_thesis/katred_thesis/Output/"+session['username']+".txt", 'r')
		temp = fileans.readline().strip("\r\n")
		answer = ""
		while ((temp!="") and (temp[0] == "(")):
			answer = answer + temp + " "
			temp = fileans.readline().strip("\r\n")
		fileans.close()
		# add query to database
		if answer != "":
			query = queries(filename, session['username'], column_x + "," + column_y + "|" + pointstr, request.form.get("submit_query"), answer, sensitivity, numberofsteps)
			db.session.add(query)
			db.session.commit()
			session['status_kmeans'] = 'success'
			return redirect(url_for('kmeans_page'))
		session['status_kmeans'] = 'error'
		return redirect(url_for('kmeans_page'))

@app.route('/help')
def viewhelp():
	session['status_cas'] = 'none'
	session['status_hist'] = 'none'
	session['status_kmeans'] = 'none'
	session['status_upload'] = 'none'
	return render_template('help.html')


@app.route('/logout')
def logout():
	allQueries = queries.query.filter_by(username=session['username']).all()
	for q in allQueries:
		db.session.delete(q)
		db.session.commit()
	session.pop('username', None)
	return redirect(url_for('index'))

if __name__=="__main__":
    app.run()
