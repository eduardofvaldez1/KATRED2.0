# write and parse data from config file
def build_file(file):
	global colnum
	global colname
	global coltype

	file.write ('#include "library-bags.fz"')

	# Pass config file name from UI
	config = open('configfile2.txt', 'r')

	colnum = int(config.readline())
	colname = config.readline().strip("\n")
	coltype = config.readline()

	config.close()
	coltype = coltype.split(',')

# header involves the censusline, i.e.  
# typedef censusline = (num,(string,(string,(string,(num,(num,(num,num)))))));
def build_header(file):
	global colnum
	global colname
	global coltype
	line = ""
	for type in coltype[:colnum-1]:
		line = line + "(" + type + ","
	line2 = ""
	for num in range(1,colnum):
		line2 = line2 + ")"
	line = line + coltype[-1] + line2 + ";"

	line = "\n\ntypedef censusline = " + line
	
	file.write(line)

# builds the let functions, i.e.
# let (age, aage) = line;
# etc.

def build_query(file, column1, var1, column2):
	global colnum
	global colname
	global coltype
	# Create function from condition
	try:
		colname = colname.split(',')
	except:
		pass
	
	let = let2 = ""
	iter = 0
	for name in colname:
		if iter == 0:
			let = "\n\tlet (" + name + ",a" + name + ") = line;"
		else:
			let2 = let2 + "\n\tlet (" + name + ",a" + name + ") = a" + colname[iter-1] + ";"
			
		if (column2):
			if colname[iter] == column2:	
				break
		elif colname[iter] == column1:	
				break
		iter+=1
		
	let = let + let2 + "\n"
	file.write(let)
	if isinstance(var1, str): var1 = '\"'+var1+'\"'
	if (column2):
		file.write("\tclip (if ("+ column1 +" == " + str(var1) + ") then { "+column2+" / LIMIT } else { 0.0 })\n}\n")
	else:
		file.write("\tclip (if ("+ column1 +" == " + str(var1) + ") then { 1.0 } else { 0.0 })\n}\n")
	

	
