# apps.members.models
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class user(db.Model):
	__tablename__ = 'users'
	username = db.Column(db.String(255), primary_key=True)
	password = db.Column(db.String(255))

	def __init__(self, username, password):
		self.username = username
		self.password = password

class dataset(db.Model):
	__tablename__ = 'datasets'
	title = db.Column(db.String(255), primary_key=True)
	description = db.Column(db.String(255))
	path = db.Column(db.String(255))
	owner = db.Column(db.String(255))

	def __init__(self, title, desc, path, owner):
		self.title = title
		self.description = desc
		self.path = path
		self.owner = owner

class configfile(db.Model):
	__tablename__ = "configfiles"
	title = db.Column(db.String(255), primary_key=True)
	colnum = db.Column(db.Integer)

	def __init__(self, title, colnum):
		self.title = title
		self.colnum = colnum

class col(db.Model):
	__tablename__ = "columns"
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	title = db.Column(db.String(255))
	columnname = db.Column(db.String(255))
	columntype = db.Column(db.Enum('num', 'string'))
	columnindex = db.Column(db.Integer)


	def __init__(self, title, columnname, coltype, colindex):
		self.title = title
		self.columnname = columnname
		self.columntype = coltype
		self.columnindex = colindex

	def serialize(self):
		return {
			'id' : self.id,
			'title' : self.title,
			'columnname' : self.columnname,
			'columntype' : self.columntype
		}


class queries(db.Model):
	__tablename__ = "queries"
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	title = db.Column(db.String(255))
	username = db.Column(db.String(255))
	querycond = db.Column(db.String(255))
	querytype = db.Column(db.Enum('Count', 'Ave', 'Sum', 'Hist', 'Kmeans'))
	queryans = db.Column(db.String(255))
	sensitivity = db.Column(db.Integer)
	steps = db.Column(db.Integer)

	def __init__(self, title, username, querycond, querytype, queryans, sensitivity, steps):
		self.title = title
		self.username = username
		self.querycond = querycond
		self.querytype = querytype
		self.queryans = queryans
		self.sensitivity = sensitivity
		self.steps = steps

class choices(db.Model):
	__tablename__ = "choices"
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	column_id = db.Column(db.Integer)
	choicename = db.Column(db.String(255))

	def __init__(self, column_id, choicename):
		self.column_id = column_id
		self.choicename = choicename
